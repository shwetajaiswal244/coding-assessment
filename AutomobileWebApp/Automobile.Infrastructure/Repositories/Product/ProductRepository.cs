﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automobile.Domain.Automobile.Repositories;
using Automobile.Domain.Models;
using Automobile.Infrastructure.DBContext;

namespace Automobile.Infrastructure.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly IAutomobileDBContext _context;

        public ProductRepository(IAutomobileDBContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get the product details
        /// </summary>
        /// <returns></returns>
        public ICollection<ProductDetails> GetProducts(string vehicleAttributes)
        {
            var productList = _context.Products
                        .Join(_context.VehicleMaster, pd => pd.ProductId, vm => vm.ProductId, (pd, vm) => new { pd, vm })
                        .Join(_context.VehicleAttributes, vma => vma.vm.AttributeMappingId, va => va.AttributeId, (vma, va) => new { vma, va })
                        .Select(m => new ProductDetails{ 
                                ProductId = m.vma.pd.ProductId, 
                                 ProductName = m.vma.pd.ProductName,
                                 ProductType = m.vma.pd.ProductType,
                                 AttributeName = m.va.AttributeName,
                                 VehicleAttributes = m.vma.vm.VehicleAttributes                            
                        }).Where(a=> a.VehicleAttributes == vehicleAttributes).ToList();

           
            return (ICollection<ProductDetails>)productList;
        }

        // Enhancement - Get the record from database master tables for different attibutes of product
        /// <summary>
        /// Get master data for Body type
        /// </summary>
        /// <returns></returns>
        public ICollection<string> GetBodyType()
        {
            var bodyTypeList =  Enum.GetValues(typeof(Body))  
                    .Cast<Body>()
                    .Select(v => v.ToString())
                    .ToList();

            return (ICollection<string>)bodyTypeList;

        }

        /// <summary>
        ///  Get master data for body paint
        /// </summary>
        /// <returns></returns>
        public ICollection<string> GetBodyPaints()
        {
            var bodyPaintList =  Enum.GetValues(typeof(BodyPaint))
                    .Cast<BodyPaint>()
                    .Select(v => v.ToString())
                    .ToList();
            return (ICollection<string>)bodyPaintList;
        }

        /// <summary>
        /// Get master data for engine
        /// </summary>
        /// <returns></returns>
        public ICollection<string> GetEngine()
        {

            var engineList = Enum.GetValues(typeof(Engine))
                    .Cast<Engine>()
                    .Select(v => v.ToString())
                    .ToList();

            return (ICollection<string>)engineList;
        }

        /// <summary>
        /// Get master data for wheels
        /// </summary>
        /// <returns></returns>
        public ICollection<string> GetWheels()
        {

           var wheelsList = Enum.GetValues(typeof(Wheels))
                    .Cast<Wheels>()
                    .Select(v => v.ToString())
                    .ToList();

            return (ICollection<string>)wheelsList;
        }

        /// <summary>
        /// Get master data for Transmission
        /// </summary>
        /// <returns></returns>
        public ICollection<string> GetTransmission()
        {
            var transmissionList =  Enum.GetValues(typeof(Transmission))
                    .Cast<Transmission>()
                    .Select(v => v.ToString())
                    .ToList();
            return (ICollection<string>)transmissionList;
        }
    }
}
