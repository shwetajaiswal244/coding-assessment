﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automobile.Domain.Automobile.Repositories;
using Automobile.Domain.Models;
using Automobile.Infrastructure.DBContext;
using Microsoft.EntityFrameworkCore;

namespace Automobile.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IAutomobileDBContext _context;

        public UserRepository(IAutomobileDBContext context)
        {
            _context = context;
        }
        public User AuthenticateUser(string username, string password)
        {
            return _context.Users.FirstOrDefault(a => a.UserName == username && a.Password == password);            
        }

        public bool UserRegistration(User userDetails)
        {
            var check = _context.Users.FirstOrDefault(s => s.UserName == userDetails.UserName);
            bool isUserAdded = false;
            if (check == null)
            {
                _context.Users.Add(userDetails);
                _context.SaveChanges();
                isUserAdded = true;
            }
            else
            {
                isUserAdded = false;
            }            
            return isUserAdded;

        }
    }
}
