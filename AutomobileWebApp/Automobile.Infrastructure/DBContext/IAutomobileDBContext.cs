﻿using Automobile.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Automobile.Infrastructure.DBContext
{
    public interface IAutomobileDBContext : IDBContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<VehicleMaster> VehicleMaster { get; set; }
        public DbSet<VehicleAttribute> VehicleAttributes { get; set; }
    }
}
