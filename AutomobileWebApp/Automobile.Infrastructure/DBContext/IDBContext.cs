using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Automobile.Infrastructure.DBContext
{

    public interface IDBContext
    {
        int SaveChanges();        
        void Dispose();
        DbSet<T> Set<T>() where T : class;
        
    }

}