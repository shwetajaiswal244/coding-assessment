﻿using Automobile.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Automobile.Infrastructure.DBContext
{
    public class AutomobileDBContext : DbContext, IAutomobileDBContext
    {
        public AutomobileDBContext(DbContextOptions<AutomobileDBContext> options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<VehicleMaster> VehicleMaster { get; set; }
        public DbSet<VehicleAttribute> VehicleAttributes { get; set; }
    }
}
