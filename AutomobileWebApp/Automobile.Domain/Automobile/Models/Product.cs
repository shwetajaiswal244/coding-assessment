﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Automobile.Domain.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }

    }

    public class VehicleMaster
    {
        [Key]
        public int VehicleMasterId { get; set; }
        public int ProductId { get; set; }
        public string VehicleAttributes { get; set; }
        public string ProductType { get; set; }
        public string AttributeMappingId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }

    }
    public class VehicleAttribute
    {
        [Key]
        public string AttributeId { get; set; }
        public string AttributeName { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
    }

    public class ProductDetails
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public string VehicleAttributes { get; set; }
        public string AttributeName { get; set; }
    }

    public enum Transmission
    {
        Automatic,
        Manual
    }

    public enum Engine
    {
        ThermalEngines,
        ElectricalEngines,
        HybridEngines,
        InternalCombustionEngines
    }

    public enum Wheels
    {
        [Description("2 Wheeler")]
        TwoWheeler = 2,
        [Description("4 Wheeler")]
        FourWheeler = 4,
        [Description("10 Wheeler")]
        TenWheeler = 10,
        [Description("16 Wheeler")]
        SixteenWheeler = 16
    }

    public enum Body
    {
        Hatchback,
        Sedan,
        SUV,
        MPV,
        MUV,
        Van,
        Minivan
    }

    public enum BodyPaint
    {
        Red,
        Green,
        White,
        Black,
        Blue,
        Gray
    }

  
}
