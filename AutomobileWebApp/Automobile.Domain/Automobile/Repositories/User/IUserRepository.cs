﻿using Automobile.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Automobile.Domain.Automobile.Repositories
{
    public interface IUserRepository
    {
        public bool UserRegistration(User user);
        public User AuthenticateUser(string username, string password);
       
    }
}
