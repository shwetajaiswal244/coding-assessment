﻿using Automobile.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Automobile.Domain.Automobile.Repositories
{
    public interface IProductRepository
    {
        public ICollection<ProductDetails> GetProducts(string vehicleAttribute);
        public ICollection<string> GetBodyType();
        public ICollection<string> GetBodyPaints();
        public ICollection<string> GetEngine();
        public ICollection<string> GetWheels();
        public ICollection<string> GetTransmission();
        
    }
}
