﻿using Automobile.Service;
using AutomobileWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using Microsoft.AspNetCore.Http;


namespace AutomobileWebApp.Controllers
{
    public class UserController : Controller
    {
        private readonly ILogger<UserController> _logger;
        private readonly IAutomobileService _automobileService;

        public UserController(IAutomobileService automobileService, ILogger<UserController> logger)
        {
            _automobileService = automobileService;
            _logger = logger;

        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult UserRegistration(UserViewModel userDetails)
        {
            if (ModelState.IsValid)
            {
                var result = _automobileService.UserRegistration(userDetails);
                if (result == true)
                {
                    ViewBag.Message = "User registered successfully!";
                }
                else
                {
                    ViewBag.Message = "Username already exists!";
                }
                return View("Register");
            }
            else
            {
                return View("Register", userDetails);
            }
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GeneratePassword()
        {
            string randomPassword = _automobileService.GenerateRandomPassword();
            return Json(randomPassword);
        }
        
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();            
            return View("Login");
        }        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginViewModel user)
        {
            if (ModelState.IsValid)
            {
                var _loggedInUser = _automobileService.AuthenticateUser(user.UserName, user.Password);                
                if (_loggedInUser.userAuthenticated == true)
                {
                    HttpContext.Session.SetString("IsLoggedInUser", "true");
                    HttpContext.Session.SetString("username", _loggedInUser.UserName);
                    //TempData["username"] = _loggedInUser.UserName;
                    return RedirectToAction("ProductDetails", "Product");
                }
                else
                {
                    ViewBag.Message = "Username/Password is incorrect.";
                    return View("Login");
                }
            }
            else
            {
                return View("Login", user);
            }
        }
    }
}
