﻿using Automobile.Service;
using AutomobileWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace AutomobileWebApp.Controllers
{
    public class ProductController : Controller
    {

        private readonly ILogger<UserController> _logger;
        private readonly IAutomobileService _automobileService;

        public ProductController(IAutomobileService automobileService, ILogger<UserController> logger)
        {
            _automobileService = automobileService;
            _logger = logger;

        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ProductDetails()
        {           
            ViewBag.BodyType = _automobileService.GetBodyType();
            ViewBag.Transmission = _automobileService.GetTransmission();
            ViewBag.Wheels = _automobileService.GetWheels();
            ViewBag.BodyPaint = _automobileService.GetBodyPaints();
            ViewBag.Engine = _automobileService.GetEngine();
            return View();
        }

        [HttpPost]
        public JsonResult SearchProduct(ProductViewModel vehicleAttribute)
        {           
            var result = _automobileService.GetProductDetails(vehicleAttribute);
            return Json(result);
        }
    }
}
