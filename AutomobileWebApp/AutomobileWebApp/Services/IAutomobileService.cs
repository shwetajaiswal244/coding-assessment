﻿using AutomobileWebApp.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Automobile.Service
{
    public interface IAutomobileService
    {
        public bool UserRegistration(UserViewModel user);
        public LoginViewModel AuthenticateUser(string username, string password);
        public List<ProductDetailsViewModel> GetProductDetails(ProductViewModel productAttributes);
        public SelectList GetBodyType();       
        public SelectList GetBodyPaints();
        public SelectList GetEngine();
        public SelectList GetWheels();
        public SelectList GetTransmission();
        public string GenerateRandomPassword();
    }
}
