﻿using Automobile.Domain.Automobile.Repositories;
using Automobile.Domain.Models;
using Automobile.Infrastructure.DBContext;
using AutomobileWebApp.Extensions;
using AutomobileWebApp.Models;
using AutomobileWebApp.Utilies;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automobile.Service
{
    public class AutomobileService : IAutomobileService
    {

        private readonly IUserRepository _userRepository;
        private readonly IProductRepository _productRepository;
        Utilities utilities = new Utilities();
        public AutomobileService(IUserRepository userRepository, IProductRepository productRepository) 
        {
            _userRepository = userRepository;
            _productRepository = productRepository;

        }
        public bool UserRegistration(UserViewModel user)
        {
            User userDetails = new User();
            userDetails.FirstName = user.FirstName;
            userDetails.LastName = user.LastName;
            userDetails.Email = user.Email;
            userDetails.UserName = user.UserName;
            userDetails.Password = user.Password;
            userDetails.CreatedBy = "Admin"; // can be changed to logged in user id 
            userDetails.CreatedOn= DateTime.Now;
            return _userRepository.UserRegistration(userDetails);
          
        }
        public LoginViewModel AuthenticateUser(string username, string password)
        {
            var user = _userRepository.AuthenticateUser(username, password);
            bool isAuthenticated = false;
            if(user != null)
            {
                isAuthenticated = true;
            }
            return user.MapToUserLoginDto(isAuthenticated); 
        }
        public string GenerateRandomPassword()
        {
            int length = 8;// generating 8 character length password can be changes based on requirement
            var randomPassword = utilities.GenerateRandomPassword(length);            
            return randomPassword;
        }        

        public List<ProductDetailsViewModel> GetProductDetails(ProductViewModel model)
        {             
            var productDetails = _productRepository.GetProducts(model.ProductAttribute);
            return productDetails.MapToProductDto();
        }

        public SelectList GetBodyType()
        {
            var bodyType = _productRepository.GetBodyType();
            return bodyType.MapToSelectListDto();
        }

        public SelectList GetBodyPaints()
        {
            var bodyPaint = _productRepository.GetBodyPaints();
            return bodyPaint.MapToSelectListDto();
        }

        public SelectList GetEngine()
        {
            var engine = _productRepository.GetEngine();
            return engine.MapToSelectListDto();
        }

        public SelectList GetWheels()
        {
            var wheels = _productRepository.GetWheels();
            return wheels.MapToSelectListDto();
        }

        public SelectList GetTransmission()
        {
            var transmission = _productRepository.GetTransmission();
            return transmission.MapToSelectListDto();
        }
    }
}
