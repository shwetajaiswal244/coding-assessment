﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomobileWebApp.Utilies
{
     public class Utilities
     {
        private const string alphaCaps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const string alphaLow = "abcdefghijklmnopqrstuvwxyz";
        private const string numerics = "1234567890";
        private const string special = "#?!@$%^*-";
        private const string allChars = alphaCaps + alphaLow + numerics + special;
        Random r = new Random();
        /// <summary>
        /// Generate the random password with above characters with specified length
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>

        public string GenerateRandomPassword(int length)
        {
            String generatedPassword = "";            
            int lowerpass, upperpass, numpass, specialchar;
            string posarray = "0123456789";
            if (length < posarray.Length)
                posarray = posarray.Substring(0, length);
            lowerpass = getRandomPosition(ref posarray);
            upperpass = getRandomPosition(ref posarray);
            numpass = getRandomPosition(ref posarray);
            specialchar = getRandomPosition(ref posarray);


            for (int i = 0; i < length; i++)
            {
                if (i == lowerpass)
                    generatedPassword += getRandomChar(alphaCaps);
                else if (i == upperpass)
                    generatedPassword += getRandomChar(alphaLow);
                else if (i == numpass)
                    generatedPassword += getRandomChar(numerics);
                else if (i == specialchar)
                    generatedPassword += getRandomChar(special);
                else
                    generatedPassword += getRandomChar(allChars);
            }
            return generatedPassword;
        }

        public string getRandomChar(string fullString)
        {
            return fullString.ToCharArray()[(int)Math.Floor(r.NextDouble() * fullString.Length)].ToString();
        }

        public int getRandomPosition(ref string posArray)
        {
            int pos;
            string randomChar = posArray.ToCharArray()[(int)Math.Floor(r.NextDouble() * posArray.Length)].ToString();
            pos = int.Parse(randomChar);
            posArray = posArray.Replace(randomChar, "");
            return pos;
        }
    }
}
