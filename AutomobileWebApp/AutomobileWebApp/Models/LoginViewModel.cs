﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AutomobileWebApp.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Please enter the username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter the password")]
        public string Password { get; set; }

        public bool userAuthenticated { get; set; }
    }
}
