﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AutomobileWebApp.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter the First name")]
        [RegularExpression(@"^[A-Z][a-zA-Z]*$", ErrorMessage = "Please enter valid First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter the Last name")]
        [RegularExpression(@"^[A-Z][a-zA-Z]*$", ErrorMessage = "Please enter valid Last name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter the Email address")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter the Username")]
        [RegularExpression("^[a-zA-Z][a-zA-Z0-9]*$", ErrorMessage = "Please enter valid Username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter the Password")]
        [RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^*-]).{8,}$", ErrorMessage = "Password should have letters(a-z), numbers(0-9) and special characters(#?!@$%^&*-)")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please enter the confirm Password")]
        [Compare("Password", ErrorMessage = "Password does not match")]
        public string ConfirmPassword { get; set; }

    }
}
