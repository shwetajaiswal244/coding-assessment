﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace AutomobileWebApp.Models
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public string ProductAttribute { get; set; }

    }
     
    public class VehicleAttributeViewModel
    {
        public string BodyType { get; set; }
        public string Transmission { get; set; }
        public string Wheels { get; set; }
        public string BodyPaint { get; set; }
        public string Dimension { get; set; }
        public string Engine { get; set; } 
       

    }

    public class ProductDetailsViewModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public string VehicleAttributes { get; set; }
        public string AttributeName { get; set; }
    }

}

    
 
        
