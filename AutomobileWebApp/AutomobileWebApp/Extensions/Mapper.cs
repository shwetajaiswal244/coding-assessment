﻿using Automobile.Domain.Models;
using AutomobileWebApp.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutomobileWebApp.Extensions 
{
    public static class Mapper
    {      
        public static LoginViewModel MapToUserLoginDto(this User user, bool isAuthenticated)
        {
            string userName = "";
            if (user != null)
            {
                userName = user.UserName;
            }
            return new LoginViewModel
            {
                userAuthenticated = isAuthenticated,
                UserName  = userName
            };
        }

        public static List<ProductDetailsViewModel> MapToProductDto(this ICollection<ProductDetails> product)
        {
            return 
                  product
                    .Select(x => new ProductDetailsViewModel()
                    {
                        ProductId = x.ProductId,
                        ProductName = x.ProductName,
                        ProductType = x.ProductType,
                        AttributeName  =  x.AttributeName,
                        VehicleAttributes = x.VehicleAttributes
                    })
                    .ToList();
        }       

        public static SelectList MapToSelectListDto(this ICollection<string> listDetails)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem()
            {
                Text = "---Select---",
                Value = ""
            });
            foreach (var data in listDetails)
            {
                list.Add(new SelectListItem()
                {
                    Text = data,
                    Value = data
                });
            }

            return new SelectList(list, "Value", "Text");
        }
    }
}
