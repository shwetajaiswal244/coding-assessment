USE [Automobile]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 05-08-2022 18:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](50) NULL,
	[ProductType] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 05-08-2022 18:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VehicleAttributes]    Script Date: 05-08-2022 18:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VehicleAttributes](
	[AttributeId] [int] IDENTITY(1,1) NOT NULL,
	[AttributeName] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_VehicleAttributes] PRIMARY KEY CLUSTERED 
(
	[AttributeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VehicleMaster]    Script Date: 05-08-2022 18:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VehicleMaster](
	[VehicleMasterId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[VehicleAttributes] [nvarchar](50) NOT NULL,
	[ProductType] [nvarchar](50) NOT NULL,
	[AttributeMappingId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_VehicleMaster] PRIMARY KEY CLUSTERED 
(
	[VehicleMasterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([ProductId], [ProductName], [ProductType], [CreatedOn], [CreatedBy]) VALUES (1, N'Product A', N'Car', CAST(N'2022-08-05T15:01:44.820' AS DateTime), N'SYSTEM')
INSERT [dbo].[Products] ([ProductId], [ProductName], [ProductType], [CreatedOn], [CreatedBy]) VALUES (2, N'Product B', N'Car', CAST(N'2022-08-05T15:03:19.360' AS DateTime), N'SYSTEM')
INSERT [dbo].[Products] ([ProductId], [ProductName], [ProductType], [CreatedOn], [CreatedBy]) VALUES (3, N'Product C', N'Truck', CAST(N'2022-08-05T15:03:38.910' AS DateTime), N'SYSTEM')
INSERT [dbo].[Products] ([ProductId], [ProductName], [ProductType], [CreatedOn], [CreatedBy]) VALUES (4, N'Product D', N'Truck', CAST(N'2022-08-05T15:12:55.243' AS DateTime), N'SYSTEM')
INSERT [dbo].[Products] ([ProductId], [ProductName], [ProductType], [CreatedOn], [CreatedBy]) VALUES (5, N'Product E', N'Bycycle', CAST(N'2022-08-05T15:12:56.660' AS DateTime), N'SYSTEM')
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [FirstName], [LastName], [Email], [Username], [Password], [CreatedOn], [CreatedBy]) VALUES (3, N'Shweta', N'Jaysaval', N'shwetajaiswal244@gmail.com', N'shweta2403', N'123456', CAST(N'2022-08-05T01:30:33.847' AS DateTime), N'Admin')
INSERT [dbo].[Users] ([Id], [FirstName], [LastName], [Email], [Username], [Password], [CreatedOn], [CreatedBy]) VALUES (4, N'Akash', N'Jaiswal', N'akash.kr.jaiswal@gmail.com', N'akash1234', N'Akash@1234', CAST(N'2022-08-05T09:43:34.817' AS DateTime), N'Admin')
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
SET IDENTITY_INSERT [dbo].[VehicleAttributes] ON 

INSERT [dbo].[VehicleAttributes] ([AttributeId], [AttributeName], [CreatedOn], [CreatedBy]) VALUES (1, N'Body', CAST(N'2022-08-05T15:04:43.337' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleAttributes] ([AttributeId], [AttributeName], [CreatedOn], [CreatedBy]) VALUES (2, N'Transmission', CAST(N'2022-08-05T15:04:50.670' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleAttributes] ([AttributeId], [AttributeName], [CreatedOn], [CreatedBy]) VALUES (3, N'Wheels', CAST(N'2022-08-05T15:04:57.720' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleAttributes] ([AttributeId], [AttributeName], [CreatedOn], [CreatedBy]) VALUES (4, N'BodyPaint', CAST(N'2022-08-05T15:05:04.393' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleAttributes] ([AttributeId], [AttributeName], [CreatedOn], [CreatedBy]) VALUES (5, N'Engine', CAST(N'2022-08-05T15:05:13.593' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleAttributes] ([AttributeId], [AttributeName], [CreatedOn], [CreatedBy]) VALUES (6, N'Dimension', CAST(N'2022-08-05T15:05:24.093' AS DateTime), N'SYSTEM')
SET IDENTITY_INSERT [dbo].[VehicleAttributes] OFF
GO
SET IDENTITY_INSERT [dbo].[VehicleMaster] ON 

INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (3, 1, N'Sedan', N'Car', 1, CAST(N'2022-08-05T15:08:49.227' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (5, 1, N'InternalCombustionEngines', N'Car', 5, CAST(N'2022-08-05T15:09:50.720' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (6, 1, N'Manual', N'Car', 2, CAST(N'2022-08-05T15:10:41.930' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (7, 1, N'Red', N'Car', 4, CAST(N'2022-08-05T15:12:04.157' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (8, 2, N'Hatchback', N'Car', 1, CAST(N'2022-08-05T15:13:56.730' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (9, 2, N'ElectricalEngines', N'Car', 5, CAST(N'2022-08-05T15:14:22.547' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (10, 2, N'Automatic', N'Car', 2, CAST(N'2022-08-05T15:14:39.120' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (11, 2, N'Black', N'Car', 4, CAST(N'2022-08-05T15:14:58.960' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (13, 3, N'TenWheeler', N'Truck', 3, CAST(N'2022-08-05T15:18:53.817' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (15, 3, N'White', N'Truck', 4, CAST(N'2022-08-05T15:19:11.827' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (18, 3, N'Manual', N'Truck', 2, CAST(N'2022-08-05T15:19:37.667' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (20, 3, N'HybridEngines', N'Truck', 5, CAST(N'2022-08-05T15:20:25.573' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (22, 3, N'1000*1400*1350', N'Truck', 6, CAST(N'2022-08-05T15:21:15.800' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (24, 4, N'SixteenWheeler', N'Truck', 3, CAST(N'2022-08-05T15:22:51.127' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (25, 4, N'Black', N'Truck', 4, CAST(N'2022-08-05T15:23:11.760' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (26, 4, N'Manual', N'Truck', 2, CAST(N'2022-08-05T15:23:27.830' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (28, 4, N'ThermalEngines', N'Truck', 5, CAST(N'2022-08-05T15:23:49.250' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (30, 5, N'TwoWheeler', N'Bycycle', 3, CAST(N'2022-08-05T15:24:19.247' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (33, 5, N'Manual', N'Bycycle', 2, CAST(N'2022-08-05T15:24:52.837' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (35, 1, N'FourWheeler', N'Car', 3, CAST(N'2022-08-05T16:06:31.587' AS DateTime), N'SYSTEM')
INSERT [dbo].[VehicleMaster] ([VehicleMasterId], [ProductId], [VehicleAttributes], [ProductType], [AttributeMappingId], [CreatedOn], [CreatedBy]) VALUES (37, 2, N'FourWheeler', N'Car', 3, CAST(N'2022-08-05T16:06:45.593' AS DateTime), N'SYSTEM')
SET IDENTITY_INSERT [dbo].[VehicleMaster] OFF
GO
ALTER TABLE [dbo].[Products] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Products_CreatedBy]  DEFAULT (N'SYSTEM') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[VehicleAttributes] ADD  CONSTRAINT [DF_VehicleAttributes_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[VehicleAttributes] ADD  CONSTRAINT [DF_VehicleAttributes_CreatedBy]  DEFAULT (N'SYSTEM') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[VehicleMaster] ADD  CONSTRAINT [DF_VehicleMaster_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[VehicleMaster] ADD  CONSTRAINT [DF_VehicleMaster_CreatedBy]  DEFAULT (N'SYSTEM') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[VehicleMaster]  WITH CHECK ADD  CONSTRAINT [FK_VehicleMaster_AttributeMapping] FOREIGN KEY([AttributeMappingId])
REFERENCES [dbo].[VehicleAttributes] ([AttributeId])
GO
ALTER TABLE [dbo].[VehicleMaster] CHECK CONSTRAINT [FK_VehicleMaster_AttributeMapping]
GO
ALTER TABLE [dbo].[VehicleMaster]  WITH CHECK ADD  CONSTRAINT [FK_VehicleMaster_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([ProductId])
GO
ALTER TABLE [dbo].[VehicleMaster] CHECK CONSTRAINT [FK_VehicleMaster_Products]
GO
